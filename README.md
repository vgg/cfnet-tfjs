# CFNet TensorflowJS

Tensorflow JS port of [CFNet](https://www.robots.ox.ac.uk/~luca/cfnet.html).

Original Matlab code - https://github.com/bertinetto/cfnet

The model and JS module is uploaded for direct usage. The steps to reproduce are also provided
to serve as a reference.

## Steps to reproduce

[convert_cfnet_mcn_to_keras.py](./convert_cfnet_mcn_to_keras.py) converts the pretrained network into a keras model.

To convert the keras model to tfjs graph, we use the official tensorflowjs-converter


```bash
# Create a virtual environment
python3 -m venv env
source env/bin/activate

# Install the dependencies
python3 -m pip install requirements.txt

# Convert the MatConvNet model to keras
python3 convert_cfnet_mcn_to_keras.py networks/cfnet-conv2_e80.mat cfnet_conv2_keras.h5

# Convert the keras model to TFJS Graph
tensorflowjs_converter \
    cfnet_conv2_keras.h5  \
    ./cfnet_tfjs \
    --input_format keras \
    --output_format tfjs_graph_model
```

The resulting model can be used with the CFNet JS module.