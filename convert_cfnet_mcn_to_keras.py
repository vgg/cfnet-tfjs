import tensorflow as tf
import numpy as np

BN_EPSILON=1e-5

def convert_bn(bn_weights):
    """
    https://github.com/albanie/pytorch-mcn/blob/c4c9bcfa727f41d62a65091b2601488c8bf04c7d/python/ptmcn_utils.py#L170
    Matconvnet stores it as moving_std = (sqrt(var + eps))
    Keras expects var
    https://github.com/tensorflow/tensorflow/blob/85c8b2a817f95a3e979ecd1ed95bff1dc1335cff/tensorflow/python/keras/backend.py#L2896
    So we Square it and subtract epsilon

    Returns 4 items (gamma, beta, mu, var)
    """
    return [
        np.ascontiguousarray(bn_weights[0]), # Gamma
        np.ascontiguousarray(bn_weights[1]), # Beta
        np.ascontiguousarray(bn_weights[2][:, 0]), # Moving mean
        (np.ascontiguousarray(bn_weights[2][:, 1]) ** 2 - BN_EPSILON)  # Moving variance
    ]

def convert_conv(conv_weights):
    """
    """
    return [
        np.ascontiguousarray(x) for x in conv_weights
    ]

def convert_grouped_conv(conv_weights):
    conv_weights = convert_conv(conv_weights)

    w1, w2 = np.split(conv_weights[0], 2, axis=-1)
    b1, b2 = np.split(conv_weights[1], 2, axis=-1)
    return [w1, b1], [w2, b2]

mapping = {
    'conv1': ['br_conv1f', 'br_conv1b'],
    'bn1': ['br_bn1m', 'br_bn1b', 'br_bn1x'],
    'conv2': ['br_conv2f', 'br_conv2b'],
    'bn2': ['br_bn2m', 'br_bn2b', 'br_bn2x'],
}
grouped = ['conv2']

def convert_mcn_params_to_keras(mcn, layers):
    net = mcn['net']

    params_list = []
    for l in mapping:
        params_list.extend(mapping[l])
    
    mcn_params = {
        param['name']: param['value'] 
        for param in net['params'] if param['name'] in params_list
    }

    keras_params = {}
    for l in layers:
        _params = [ mcn_params[p] for p in mapping[l] ]
        if l.startswith('conv'):
            if l in grouped:
                keras_params[f'{l}_1'], keras_params[f'{l}_2'] = convert_grouped_conv(_params)
            else:
                keras_params[l] = convert_conv(_params)
        elif l.startswith('bn'):
            keras_params[l] = convert_bn(_params)
        else:
            raise Exception('Unknown layer')

    return keras_params

def group_conv(x, kernel_size, strides, filters, name):
    splits = tf.split(x, 2, axis=-1)

    x1 = tf.keras.layers.Conv2D(
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            name=f'{name}_1')(splits[0])
    x2 = tf.keras.layers.Conv2D(
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            name=f'{name}_2')(splits[1])

    return tf.keras.layers.Concatenate(axis=-1, name=f'{name}_concat')([x1, x2])

def get_cfnet_model(input_size, trainable=False):

    inputs = tf.keras.layers.Input(shape=input_size)

    # Conv1 (11 x 11 x 3 x 96)
    x = tf.keras.layers.Conv2D(
        filters=96,
        kernel_size=(11, 11),
        strides=(2, 2),
        input_shape=input_size,
        name='conv1',
    )(inputs)

    # Batchnorm
    x = tf.keras.layers.BatchNormalization(
        epsilon=BN_EPSILON,
        name='bn1',
    )(x)

    # Relu
    x = tf.keras.layers.ReLU(
        name='relu1',
    )(x)

    # Max Pool 2d
    x = tf.keras.layers.MaxPool2D(
        pool_size=(3, 3),
        strides=(2, 2),
        name='pool1',
    )(x)

    # Conv 2 - Grouped conv - must be implemented as split -> conv -> concat
    # (5 x 5 x 48 x 32)
    # Split
    x = group_conv(x, (5, 5), (1, 1), 16, 'conv2')

    # Batchnorm
    x = tf.keras.layers.BatchNormalization(
        name='bn2',
        epsilon=BN_EPSILON,
    )(x)

    # Relu
    x = tf.keras.layers.ReLU(
        name='relu2'
    )(x)

    model = tf.keras.Model(inputs, x)
    model.trainable = trainable

    return model
    
if __name__ == '__main__':

    import argparse
    import scipy.io

    parser = argparse.ArgumentParser()
    parser.add_argument('mcn_path', help='MatConvNet CFNet 2 layer model path')
    parser.add_argument('output', help='Output path to store the converted model')
    args = parser.parse_args()

    mcn = scipy.io.loadmat(args.mcn_path, simplify_cells=True)
    # '../../networks/cfnet-conv2_e80.mat'
    model = get_cfnet_model((255, 255, 3))
    layers = ['conv1', 'bn1', 'conv2', 'bn2']
    keras_params = convert_mcn_params_to_keras(mcn, layers)
    for layer in keras_params:
        l = model.get_layer(layer)
        l.set_weights(keras_params[layer])

    print(model.summary())

    model.save(args.output)
